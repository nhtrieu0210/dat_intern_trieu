import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Header from "./components/Header/Header";
import Sidebar from "./components/Sidebar/Sidebar";
import Dashboard from "./components/Dashboard/Dashboard";

function App() {
  return (
      <div className="App">
        <div className="layout">
          <Router>
            <Header/>
            <div className="layout__container">
              <Sidebar/>
              <Routes>
                <Route exact path="/" element={<Dashboard/>}/>
              </Routes>
            </div>
          </Router>
        </div>
      </div>
  );
}

export default App;
