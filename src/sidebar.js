const sideBarValue = [
    {
        name : "Core",
        children : [
            {
                name: "Dashboards",
                children: [
                    {
                        name: "Default"
                    },
                    {
                        name: "Multipurpose"
                    },
                    {
                        name: "Affiliate"
                    }
                ]
            },
        ],
    },
    {
        name : "Custom",
        children: [
            {
                name: "Pages",
                children: [
                    {
                        name: "Account",
                        children: [
                            {
                                name : "Profile"
                            },
                            {
                                name : "Billing"
                            },
                            {
                                name : "Security"
                            },
                            {
                                name : "Notifications"
                            },
                        ]
                    },
                    {
                        name: "Authentication",
                        children: [
                            {
                                name : "Basic"
                            },
                            {
                                name : "Social"
                            }
                        ]
                    },
                    {
                        name: "Error",
                        children: [
                            {
                                name : "400 Error"
                            },
                            {
                                name : "401 Error"
                            },
                            {
                                name : "403 Error"
                            },
                            {
                                name : "404 Error"
                            }
                        ]
                    },
                    {
                        name: "Pricing",
                    },
                    {
                        name: "Invoice",
                    }
                ]
            }

        ]
    }
]