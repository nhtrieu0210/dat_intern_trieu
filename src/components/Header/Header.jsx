import {
    FiActivity, FiAlertTriangle,
    FiBarChart,
    FiBell,
    FiBook,
    FiChevronRight,
    FiCode,
    FiFileText, FiLogOut,
    FiMail,
    FiMenu,
    FiSearch, FiSettings, FiUserPlus
} from "react-icons/fi";
import Profile1 from "../../assets/image/profile-1.png";
import Profile2 from "../../assets/image/profile-2.png";
import Profile3 from "../../assets/image/profile-3.png";
import Profile4 from "../../assets/image/profile-4.png";
import Profile5 from "../../assets/image/profile-5.png";
import {useEffect, useState} from "react";

import "@fontsource/metropolis/400.css";
import {signal} from "@preact/signals";

export let signalSideBar = signal(true);

function Header() {
    let [isOpenSidebar, setOpenSidebar] = useState(false);
    let [isOpenDocument, setOpenDocument] = useState(false);
    let [isOpenNav, setOpenNav] = useState(null)

    const handleOpenSidebar = () => {
        setOpenSidebar(!isOpenSidebar)
        signalSideBar.value = isOpenSidebar;
    };

    const handleOpenNav = (index) => {
        setOpenDocument(!isOpenDocument);
        setOpenNav(isOpenNav === index ? null : index);
    };

    const handleCloseNav = () => {
        setOpenNav(false)
    }
    useEffect(() => {
        if (isOpenNav   ) {
            document.addEventListener('mousedown', handleCloseNav);
        } else {
            document.removeEventListener('mousedown', handleCloseNav);
        }

        return () => {
            document.removeEventListener('mousedown', handleCloseNav);
        };
    }, [isOpenNav]);

    return (
        <>
            <header className="Header">
                <div onClick={handleOpenSidebar}  className="Header_Icon">
                    <div id="iconSideBar">
                        <FiMenu size={14} color={"#a1a3a7"}/>
                    </div>
                </div>
                <div className="Header_Brand">
                    <div>SB Admin Pro</div>
                </div>
                <div className="Header_Search">
                    <input type="text" placeholder="Search" />
                    <button>
                        <FiSearch size={16}/>
                    </button>
                </div>
                <ul className="Header_Nav">
                    <li className="Header_Nav_Item">
                        <div className="Header_Nav_Item_Document">
                            <div className="Header_Nav_Item_Document_Content" onClick={() => handleOpenNav(1)}>
                                <span className="Header_Nav_Item_Document_Content_Text">
                                    Documentation
                                </span>
                                <div id="DocumentArrow" className={isOpenDocument ? "Header_Nav_Item_Document_Content_Icon rotate" : "Header_Nav_Item_Document_Content_Icon"}>
                                    <FiChevronRight size={14} color="#8d8d8d"/>
                                </div>
                            </div>
                            {isOpenNav === 1 && <div className="Header_Nav_Item_Document_Popup" id="PopupDocument">
                                <div className="Header_Nav_Item_Document_Popup_Container">
                                    <div className="Header_Nav_Item_Document_Popup_Container_Icon">
                                        <FiBook size={16} color={"#0061f2"}/>
                                    </div>
                                    <div className="Header_Nav_Item_Document_Popup_Container_Content">
                                        <div className="Header_Nav_Item_Document_Popup_Container_Content_Text">
                                            <div className="Title">
                                                Documentation
                                            </div>
                                            <div className="Subtitle">
                                                Usage instrucions and reference
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="Header_Nav_Item_Document_Popup_Container">
                                    <div className="Header_Nav_Item_Document_Popup_Container_Icon">
                                        <FiCode size={16} color={"#0061f2"}/>
                                    </div>
                                    <div className="Header_Nav_Item_Document_Popup_Container_Content">
                                        <div className="Header_Nav_Item_Document_Popup_Container_Content_Text">
                                            <div className="Title">
                                                Components
                                            </div>
                                            <div className="Subtitle">
                                                Code snippets and reference
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="Header_Nav_Item_Document_Popup_Container">
                                    <div className="Header_Nav_Item_Document_Popup_Container_Icon">
                                        <FiFileText size={16} color={"#0061f2"}/>
                                    </div>
                                    <div className="Header_Nav_Item_Document_Popup_Container_Content">
                                        <div className="Header_Nav_Item_Document_Popup_Container_Content_Text">
                                            <div className="Title">
                                                Changelog
                                            </div>
                                            <div className="Subtitle">
                                                Updates and changes
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>}
                        </div>
                    </li>
                    <li className="Header_Nav_Item Alert">
                        <div href="" role="button" onClick={() => handleOpenNav(2)} className="Header_Nav_Item_Icon ">
                            <FiBell size={14} color="#8d8d8d"/>
                        </div>
                        {isOpenNav === 2 &&
                            <div className="Alert_Popup">
                                <div className="Alert_Popup_Header">
                                    <div>
                                        <FiBell color={"rgba(255, 255, 255, 0.7)"} size={11}/>
                                    </div>
                                    <div>
                                        Alerts Center
                                    </div>
                                </div>
                                <div className="Alert_Popup_Container">
                                    <a className="Alert_Popup_Container_Item">
                                        <div className="Alert_Popup_Container_Item_Icon Activity">
                                            <FiActivity size={16}/>
                                        </div>
                                        <div className="Alert_Popup_Container_Item_Content">
                                            <div className="Date">
                                                December 29, 2021
                                            </div>
                                            <div className="Message">
                                                This is an alert message. It's nothing serious, but it requires your
                                                attention.
                                            </div>
                                        </div>
                                    </a>
                                    <a className="Alert_Popup_Container_Item">
                                        <div className="Alert_Popup_Container_Item_Icon Barchart">
                                            <FiBarChart size={16}/>
                                        </div>
                                        <div className="Alert_Popup_Container_Item_Content">
                                            <div className="Date">
                                                December 22, 2021
                                            </div>
                                            <div className="Message">
                                                A new monthly report is ready. Click here to view!
                                            </div>
                                        </div>
                                    </a>
                                    <a className="Alert_Popup_Container_Item">
                                        <div className="Alert_Popup_Container_Item_Icon Danger">
                                            <FiAlertTriangle size={16}/>
                                        </div>
                                        <div className="Alert_Popup_Container_Item_Content">
                                            <div className="Date">
                                                December 22, 2021
                                            </div>
                                            <div className="Message">
                                                Critical system failure, systems shutting down.
                                            </div>
                                        </div>
                                    </a>
                                    <a className="Alert_Popup_Container_Item">
                                        <div className="Alert_Popup_Container_Item_Icon Request">
                                            <FiUserPlus size={16}/>
                                        </div>
                                        <div className="Alert_Popup_Container_Item_Content">
                                            <div className="Date">
                                                December 22, 2021
                                            </div>
                                            <div className="Message">
                                                New user request. Woody has requested access to the organization.
                                            </div>
                                        </div>
                                    </a>
                                    <a className="Alert_Popup_Container_ViewAll">
                                        View All Alerts
                                    </a>
                                </div>
                            </div>
                        }

                    </li>
                    <li className="Header_Nav_Item Message">
                        <div href="" role="button" onClick={() => handleOpenNav(3)}  className="Header_Nav_Item_Icon">
                            <FiMail size={14} color="#8d8d8d"/>
                        </div>
                        {isOpenNav === 3 &&
                             <div className="Message_Popup">
                                <div className="Message_Popup_Header">
                                    <div>
                                        <FiMail color={"rgba(255, 255, 255, 0.7)"} size={11}/>
                                    </div>
                                    <div>
                                        Message Center
                                    </div>
                                </div>
                                <div className="Alert_Popup_Container">
                                    <div className="Message_Popup_Container_Item">
                                        <a href='' role="button" className="Message_Popup_Container_Item_Image">
                                            <img src={Profile2} alt=""/>
                                        </a>
                                        <div className="Message_Popup_Container_Item_Content">
                                            <div className="Message">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo,
                                                quam?
                                            </div>
                                            <div className="Name_Time">
                                                Thomas Wilcox · 58min
                                            </div>
                                        </div>
                                    </div>
                                    <a className="Message_Popup_Container_Item">
                                        <div className="Message_Popup_Container_Item_Image">
                                            <img src={Profile3} alt=""/>
                                        </div>
                                        <div className="Message_Popup_Container_Item_Content">
                                            <div className="Message">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo,
                                                quam?
                                            </div>
                                            <div className="Name_Time">
                                                Emily Fowler · 2d
                                            </div>
                                        </div>
                                    </a>
                                    <a className="Message_Popup_Container_Item">
                                        <div className="Message_Popup_Container_Item_Image">
                                            <img src={Profile4} alt=""/>
                                        </div>
                                        <div className="Message_Popup_Container_Item_Content">
                                            <div className="Message">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo,
                                                quam?
                                            </div>
                                            <div className="Name_Time">
                                                Marshall Rosencrantz · 3d
                                            </div>
                                        </div>
                                    </a>
                                    <a className="Message_Popup_Container_Item">
                                        <div className="Message_Popup_Container_Item_Image">
                                            <img src={Profile5} alt=""/>
                                        </div>
                                        <div className="Message_Popup_Container_Item_Content">
                                            <div className="Message">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo,
                                                quam?
                                            </div>
                                            <div className="Name_Time">
                                                Colby Newton · 3d
                                            </div>
                                        </div>
                                    </a>

                                    <a className="Alert_Popup_Container_ViewAll">
                                        Read All Messages
                                    </a>
                                </div>
                            </div>
                        }
                    </li>
                    <li className="Header_Nav_Item Profile">
                        <div role="button" onClick={() => handleOpenNav(4)} className="Header_Nav_Item_Icon">
                            <img className="img" src={Profile1} alt=""/>
                        </div>
                        {isOpenNav === 4 &&
                            <div className="Profile_Popup">
                                <div className="Profile_Popup_Header">
                                    <div className="Profile_Popup_Header_Img">
                                        <img src={Profile1} alt=""/>
                                    </div>
                                    <div className="Profile_Popup_Header_Content">
                                        <div className="Profile_Popup_Header_Content_Name">
                                            Valerie Luna
                                        </div>
                                        <div className="Profile_Popup_Header_Content_Email">
                                            vluna@aol.com
                                        </div>
                                    </div>
                                </div>
                                <div className="Profile_Popup_Divided"></div>
                                <div className="Profile_Popup_Content">
                                    <div className="Profile_Popup_Content_Account">
                                        <div className="icon">
                                            <FiSettings size={12} color="#69707a"/>
                                        </div>
                                        Account
                                    </div>
                                    <div className="Profile_Popup_Content_Logout">
                                        <div className="icon">
                                            <FiLogOut size={12}  color="#69707a"/>
                                        </div>
                                        Logout
                                    </div>
                                </div>
                            </div>
                        }
                    </li>
                </ul>
            </header>
        </>
    );
};

export default Header;
