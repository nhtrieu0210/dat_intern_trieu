import {FiActivity, FiChevronRight, FiGlobe, FiGrid, FiLayout, FiPackage, FiRepeat, FiTool} from "react-icons/fi";
import "@fontsource/metropolis/500.css";

import {signalSideBar} from "../Header/Header";
import {useEffect, useRef, useState} from "react";

const Sidebar = () => {
    let [isOpenSidebar, setOpenSidebar] = useState(false);
    let [isOpenSubmenu, setOpenSubmenu] = useState(null);
    let [isOpenSubmenuChild, setOpenSubmenuChild] = useState(null);
    let [isOpenMenu, setOpenMenu] = useState(null);

    useEffect(() => {
        const unsubcribe =
            signalSideBar.subscribe((value)=> {
                setOpenSidebar(value)
            }
        )
        return unsubcribe;
    }, [signalSideBar]);

    const handleOpenMenu = (index) => {
        setOpenMenu(isOpenMenu === index ? null : index);
    }

    const handleOpenSubmenu = (index) => {
        setOpenSubmenu(isOpenSubmenu === index ? null : index);
    }

    const handleOpenSubmenuChild = (index) => {
        setOpenSubmenuChild(isOpenSubmenuChild === index ? null : index);
    }

    return (
        <>
            <div id="Sidebar"
                className={`Sidebar ${isOpenSidebar ? '' : 'hidden'}`}
            >
                <div className="Sidebar_Container">
                    <div className="Sidebar_Container_List">
                        <div className="Sidebar_Container_List_Item">
                            <div className="Sidebar_Container_List_Item_Heading">
                                Core
                            </div>
                            <div onClick={() => handleOpenMenu(1)}  className="Sidebar_Container_List_Item_Title active">
                                <div className="Icon">
                                    <FiActivity size={16}/>
                                </div>
                                Dashboards
                                <div className="Icon">
                                    <div>
                                        <FiChevronRight size={18}/>
                                    </div>
                                </div>
                            </div>
                            {isOpenMenu === 1 &&
                                <nav className="Submenu">
                                    <div className="Submenu_Item">
                                        Default
                                    </div>
                                    <div className="Submenu_Item">
                                        Multipurpose
                                    </div>
                                    <div className="Submenu_Item">
                                        Affiliate
                                    </div>
                                </nav>
                            }
                        </div>
                        <div className="Sidebar_Container_List_Item">
                            <div className="Sidebar_Container_List_Item_Heading">
                                Custom
                            </div>
                            <div onClick={() => handleOpenMenu(2)} className="Sidebar_Container_List_Item_Title">
                                <div className="Icon">
                                    <FiGrid size={16}/>
                                </div>
                                Pages
                                <div className="Icon">
                                    <div>
                                        <FiChevronRight size={18}/>
                                    </div>
                                </div>
                            </div>
                                {isOpenMenu === 2 &&
                                    <nav className="Submenu">
                                        <div onClick={() => handleOpenSubmenu(21)} className="Submenu_Item">
                                            Account
                                            <span className="Icon">
                                                <FiChevronRight size={18}/>
                                            </span>
                                        </div>
                                        {isOpenSubmenu === 21 &&
                                            <nav className="Submenu">
                                            <div className="Submenu_Item">
                                                Profile
                                            </div>
                                            <div className="Submenu_Item">
                                                Billing
                                            </div>
                                            <div className="Submenu_Item">
                                                Security
                                            </div>
                                            <div className="Submenu_Item">
                                                Notifications
                                            </div>
                                        </nav>
                                    }
                                        <div onClick={() => handleOpenSubmenu(22)} className="Submenu_Item">
                                            Authentication
                                            <span className="Icon">
                                                <FiChevronRight size={18}/>
                                            </span>
                                        </div>
                                        {isOpenSubmenu === 22 &&
                                            <nav className="Submenu">
                                            <div onClick={() => handleOpenSubmenuChild(211)}
                                                     className="Submenu_Item">
                                                    Basic
                                                    <span className="Icon">
                                                        <FiChevronRight size={18}/>
                                                    </span>
                                                </div>
                                                {isOpenSubmenuChild === 211 &&
                                                    <nav className="Submenu">
                                                        <div className="Submenu_Item">
                                                            Login
                                                        </div>
                                                        <div className="Submenu_Item">
                                                            Register
                                                        </div>
                                                        <div className="Submenu_Item">
                                                            Forget Password
                                                        </div>
                                                    </nav>
                                                }
                                                <div onClick={() => handleOpenSubmenuChild(212)}
                                                     className="Submenu_Item">
                                                    Social
                                                    <span className="Icon">
                                                        <FiChevronRight size={18}/>
                                                    </span>
                                                </div>
                                                {isOpenSubmenuChild === 212 &&
                                                    <nav className="Submenu">
                                                        <div className="Submenu_Item">
                                                            Login
                                                        </div>
                                                        <div className="Submenu_Item">
                                                            Register
                                                        </div>
                                                        <div className="Submenu_Item">
                                                            Forget Password
                                                        </div>
                                                    </nav>
                                                }
                                            </nav>
                                        }
                                        <div onClick={() => handleOpenSubmenu(23)} className="Submenu_Item">
                                            Error
                                            <span className="Icon">
                                                <FiChevronRight size={18}/>
                                            </span>
                                        </div>
                                        {isOpenSubmenu === 23 &&
                                            <nav className="Submenu">
                                            <div className="Submenu_Item">
                                                    400 Error
                                                </div>
                                                <div className="Submenu_Item">
                                                    401 Error
                                                </div>
                                                <div className="Submenu_Item">
                                                    402 Error
                                                </div>
                                                <div className="Submenu_Item">
                                                    403 Error
                                                </div>
                                                <div className="Submenu_Item">
                                                    404 Error
                                                </div>
                                            </nav>
                                        }
                                        <div className="Submenu_Item">
                                            Pricing
                                        </div>
                                        <div className="Submenu_Item">
                                            Invoice
                                        </div>
                                    </nav>
                                }
                            <div onClick={() => handleOpenMenu(3)} className="Sidebar_Container_List_Item_Title">
                                <div className="Icon">
                                    <FiGlobe size={16}/>
                                </div>
                                Applications
                                <div className="Icon">
                                    <div>
                                        <FiChevronRight size={18}/>
                                    </div>
                                </div>
                            </div>
                            {isOpenMenu === 3 &&
                                <nav className="Submenu">
                                    <div onClick={() => handleOpenSubmenu(31)} className="Submenu_Item">
                                        Knowledge Base
                                        <span className="Icon">
                                            <FiChevronRight size={18}/>
                                        </span>
                                    </div>
                                    {isOpenSubmenu === 31 &&
                                        <nav className="Submenu">
                                        <div className="Submenu_Item">
                                                Home 1
                                            </div>
                                            <div className="Submenu_Item">
                                                Home 2
                                            </div>
                                            <div className="Submenu_Item">
                                                Category
                                            </div>
                                            <div className="Submenu_Item">
                                                Article
                                            </div>
                                        </nav>
                                    }
                                    <div onClick={() => handleOpenSubmenu(32)} className="Submenu_Item">
                                        User Management
                                        <span className="Icon">
                                            <FiChevronRight size={18}/>
                                        </span>
                                    </div>
                                    {isOpenSubmenu === 32 &&
                                        <nav className="Submenu">
                                            <div className="Submenu_Item">
                                                User List
                                            </div>
                                            <div href="" className="Submenu_Item">
                                                Edit User
                                            </div>
                                            <div className="Submenu_Item">
                                                Add User
                                            </div>
                                            <div className="Submenu_Item">
                                                Groups List
                                            </div>
                                            <div className="Submenu_Item">
                                                Organization Details
                                            </div>
                                        </nav>
                                    }
                                    <div onClick={()=> handleOpenSubmenu(33)} className="Submenu_Item">
                                        Posts Management <FiChevronRight size={18}/>
                                    </div>
                                    {isOpenSubmenu === 33 &&
                                        <nav className="Submenu">
                                            <div className="Submenu_Item">
                                                Posts List
                                            </div>
                                            <div className="Submenu_Item">
                                                Create Post
                                            </div>
                                            <div className="Submenu_Item">
                                                Edit Post
                                            </div>
                                            <div className="Submenu_Item">
                                                Post Admin
                                            </div>
                                        </nav>
                                    }
                                </nav>
                            }
                            <div onClick={() => handleOpenMenu(4)} className="Sidebar_Container_List_Item_Title">
                                <div className="Icon">
                                    <FiRepeat size={16}/>
                                </div>
                                Flows
                                <div className="Icon">
                                    <div>
                                        <FiChevronRight size={18}/>
                                    </div>
                                </div>
                            </div>
                            {isOpenMenu === 4 &&
                                <nav className="Submenu">
                                    <div className="Submenu_Item">
                                        Multi-Tenant Registration
                                    </div>
                                    <div className="Submenu_Item">
                                        Wizard
                                    </div>
                                </nav>
                            }
                        </div>
                        <div className="Sidebar_Container_List_Item">
                            <div className="Sidebar_Container_List_Item_Heading">
                                UI Toolkit
                            </div>
                            <div onClick={() => handleOpenMenu(5)} className="Sidebar_Container_List_Item_Title">
                                <div className="Icon">
                                    <FiLayout size={16}/>
                                </div>
                                Layout
                                <div className="Icon">
                                    <div>
                                        <FiChevronRight size={18}/>
                                    </div>
                                </div>
                            </div>
                            {isOpenMenu === 5 &&
                                <nav className="Submenu">
                                    <div onClick={() => handleOpenSubmenu(51)} className="Submenu_Item">
                                        Navigation
                                        <span className="Icon">
                                            <FiChevronRight size={18}/>
                                        </span>
                                    </div>
                                    {isOpenSubmenu === 51 &&
                                        <nav className="Submenu">
                                            <div className="Submenu_Item">
                                                Static Sidenav
                                            </div>
                                            <div className="Submenu_Item">
                                                Dark Sidenav
                                            </div>
                                            <div className="Submenu_Item">
                                                RTL Sidenav
                                            </div>
                                        </nav>
                                    }
                                    <div onClick={() => handleOpenSubmenu(52)} className="Submenu_Item">
                                        Container Options
                                        <span className="Icon">
                                            <FiChevronRight size={18}/>
                                        </span>
                                    </div>
                                    {isOpenSubmenu === 52 &&
                                        <nav className="Submenu">
                                            <div className="Submenu_Item">
                                                Boxed Layout
                                            </div>
                                            <div className="Submenu_Item">
                                                Fluid Layout
                                            </div>
                                        </nav>
                                    }
                                    <div onClick={() => handleOpenSubmenu(53)} className="Submenu_Item">
                                        Page Headers
                                        <span className="Icon">
                                            <FiChevronRight size={18}/>
                                        </span>
                                    </div>
                                    {isOpenSubmenu === 53 &&
                                        <nav className="Submenu">
                                            <div className="Submenu_Item">
                                                Simplified
                                            </div>
                                            <div className="Submenu_Item">
                                                Compact
                                            </div>
                                            <div className="Submenu_Item">
                                                Content Overlap
                                            </div>
                                            <div className="Submenu_Item">
                                                Breadcrumbs
                                            </div>
                                            <div className="Submenu_Item">
                                                Light
                                            </div>
                                        </nav>
                                    }
                                    <div onClick={() => handleOpenSubmenu(54)} className="Submenu_Item">
                                        Starter Layouts
                                        <span className="Icon">
                                            <FiChevronRight size={18}/>
                                        </span>
                                    </div>
                                    {isOpenSubmenu === 54 &&
                                        <nav className="Submenu">
                                            <div className="Submenu_Item">
                                                Default
                                            </div>
                                            <div className="Submenu_Item">
                                                Minimal
                                            </div>
                                        </nav>
                                    }
                                </nav>
                            }
                            <div onClick={() => handleOpenMenu(6)} className="Sidebar_Container_List_Item_Title">
                                <div className="Icon">
                                    <FiPackage size={16}/>
                                </div>
                                Components
                                <div className="Icon">
                                    <div>
                                        <FiChevronRight size={18}/>
                                    </div>
                                </div>
                            </div>
                            {isOpenMenu === 6 &&
                                <nav className="Submenu">
                                    <div className="Submenu_Item">
                                        Alerts
                                    </div>
                                    <div className="Submenu_Item">
                                        Avatars
                                    </div>
                                    <div className="Submenu_Item">
                                        Badges
                                    </div>
                                    <div className="Submenu_Item">
                                        Buttons
                                    </div>
                                    <div className="Submenu_Item">
                                        Cards
                                    </div>
                                    <div className="Submenu_Item">
                                        Dropdowns
                                    </div>
                                    <div className="Submenu_Item">
                                        Forms
                                    </div>
                                    <div className="Submenu_Item">
                                        Modals
                                    </div>
                                    <div className="Submenu_Item">
                                        Navigations
                                    </div>
                                    <div className="Submenu_Item">
                                        Progress
                                    </div>
                                    <div className="Submenu_Item">
                                        Step
                                    </div>
                                    <div className="Submenu_Item">
                                        Timelines
                                    </div>
                                    <div className="Submenu_Item">
                                        Toasts
                                    </div>
                                    <div className="Submenu_Item">
                                        Tooltips
                                    </div>
                                </nav>
                            }
                            <div onClick={() => handleOpenMenu(7)} className="Sidebar_Container_List_Item_Title">
                                <div className="Icon">
                                    <FiTool size={16}/>
                                </div>
                                Utilities
                                <div className="Icon">
                                    <div>
                                    <FiChevronRight size={18}/>
                                    </div>
                                </div>
                            </div>
                            {isOpenMenu === 7 &&
                                <nav className="Submenu">
                                    <div className="Submenu_Item">
                                        Animations
                                    </div>
                                    <div className="Submenu_Item">
                                        Backgrounds
                                    </div>
                                    <div className="Submenu_Item">
                                        Borders
                                    </div>
                                    <div className="Submenu_Item">
                                        Lift
                                    </div>
                                    <div className="Submenu_Item">
                                        Shadows
                                    </div>
                                    <div className="Submenu_Item">
                                        Typography
                                    </div>
                                </nav>
                            }
                        </div>
                    </div>
                </div>
                <div className="Sidebar_Footer">
                    <div className="Sidebar_Footer_Content">
                        <div className="Sidebar_Footer_Content_Subtitle">Logged in as:</div>
                        <div className="Sidebar_Footer_Content_Title">Valerie Luna</div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Sidebar;