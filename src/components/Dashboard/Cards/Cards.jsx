import {FiCalendar, FiCheckSquare, FiChevronRight, FiDollarSign, FiMessageCircle} from "react-icons/fi";

const Cards = () => {
    return (
        <>
            <div className="Dashboard_Card_Container_Item">
                <div className="Card Primary">
                    <div className="Card_Body">
                        <div className="Card_Body_Content">
                            <div className="Card_Body_Content_Left">
                                <div className="Card_Body_Content_Left_Heading">
                                    Earning (Monthly)
                                </div>
                                <div className="Card_Body_Content_Left_Desc">
                                    $40,000
                                </div>
                            </div>
                            <div className="Card_Body_Content_Right">
                                <FiCalendar color={"rgba(255,255,255,0.6)"} size={40}/>
                            </div>
                        </div>
                    </div>
                    <div className="Card_Footer">
                        <a className="Card_Footer_Text">
                            View Report
                        </a>
                        <div className="Card_Footer_Icon">
                            <FiChevronRight size={14}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="Dashboard_Card_Container_Item">
                <div className="Card Warning">
                    <div className="Card_Body">
                        <div className="Card_Body_Content">
                            <div className="Card_Body_Content_Left">
                                <div className="Card_Body_Content_Left_Heading">
                                    Earning (Annual)
                                </div>
                                <div className="Card_Body_Content_Left_Desc">
                                    $215,000
                                </div>
                            </div>
                            <FiDollarSign color={"rgba(255,255,255,0.6)"} size={40}/>
                        </div>
                    </div>
                    <div className="Card_Footer">
                        <a className="Card_Footer_Text">
                            View Report
                        </a>
                        <FiChevronRight size={14}/>
                    </div>
                </div>
            </div>
            <div className="Dashboard_Card_Container_Item">
                <div className="Card Success">
                    <div className="Card_Body">
                        <div className="Card_Body_Content">
                            <div className="Card_Body_Content_Left">
                                <div className="Card_Body_Content_Left_Heading">
                                    Task Completion
                                </div>
                                <div className="Card_Body_Content_Left_Desc">
                                    24
                                </div>
                            </div>
                            <FiCheckSquare color={"rgba(255,255,255,0.6)"} size={40}/>
                        </div>
                    </div>
                    <div className="Card_Footer">
                        <a className="Card_Footer_Text">
                            View Tasks
                        </a>
                        <FiChevronRight size={14}/>
                    </div>
                </div>
            </div>
            <div className="Dashboard_Card_Container_Item">
                <div className="Card Danger">
                    <div className="Card_Body">
                        <div className="Card_Body_Content">
                            <div className="Card_Body_Content_Left">
                                <div className="Card_Body_Content_Left_Heading">
                                    Pending Requests
                                </div>
                                <div className="Card_Body_Content_Left_Desc">
                                    17
                                </div>
                            </div>
                            <FiMessageCircle color={"rgba(255,255,255,0.6)"} size={40}/>
                        </div>
                    </div>
                    <div className="Card_Footer">
                        <a className="Card_Footer_Text">
                            View Requests
                        </a>
                        <FiChevronRight size={14}/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Cards;