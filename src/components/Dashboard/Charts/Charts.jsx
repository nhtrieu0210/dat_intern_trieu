import {FiMoreVertical} from "react-icons/fi";

import CanvasJSReact from '@canvasjs/react-charts';
const CanvasJSChart = CanvasJSReact.CanvasJSChart;

const Charts = () => {

    const optionChartSpline = {
        animationEnabled: true,
        theme: "light2",
        axisX: {
            valueFormatString: "MMM",
            interval: 1,
            labelFormatter: function (e) {
                return e.label ? e.label : "";
            }
        },
        axisY: {
            prefix: "$"
        },
        data: [{
            color: "#0061f2",
            indexLabelFontColor: "#0061f2",
            yValueFormatString: "$#,###",
            xValueFormatString: "MMMM",
            type: "spline",
            dataPoints: [
                { label: "Jan", y: 0 },
                { y: 10000 },
                { label: "Mar", y: 5000 },
                { y: 15000 },
                { label: "May", y: 10000 },
                { y: 20000 },
                { label: "Jul", y: 15000 },
                { y: 25000 },
                { label: "Sep", y: 20000 },
                { y: 40000 },
            ]
        }]
    };

    const optionChartLabel = {
        animationEnabled: true,
        exportEnabled: false,
        theme: "light2",
        axisY: {
            includeZero: true,
            prefix: "$"
        },
        dataPointWidth: 25,
        data: [{
            color: "#0061f2",
            indexLabelFontColor: "#0061f2",
            yValueFormatString: "$#,###",
            indexLabelPlacement: "outside",
            dataPoints: [
                { label: "January", y: 4215 },
                { label: "March", y: 6251 },
                { label: "February", y: 5312 },
                { label: "May", y: 9821 },
                { label: "April", y: 7841 },
                { label: "June", y: 14984 }
            ]
        }]
    };

    return (
        <>
            <div className="Dashboard_Chart_Container">
                <div className="Dashboard_Chart_Container_Item">
                    <div className="Dashboard_Chart_Container_Item_Header">
                        <div className="Title">
                            Earnings Breakdown
                        </div>
                        <div className="Icon">
                            <FiMoreVertical size={16} color={"rgba(33, 40, 50, 0.5)"}/>
                        </div>
                    </div>
                    <div className="Dashboard_Chart_Container_Item_Body">
                        <CanvasJSChart
                            className="Chart"
                            options={optionChartSpline}
                            containerProps={{
                                width: '100%',
                                height: '100%'
                            }}
                        />
                    </div>
                </div>
                <div className="Dashboard_Chart_Container_Item">
                    <div className="Dashboard_Chart_Container_Item_Header">
                        <div className="Title">
                            Monthly Revenue
                        </div>
                        <div className="Icon">
                            <FiMoreVertical size={16} color={"rgba(33, 40, 50, 0.5)"}/>
                        </div>
                    </div>
                    <div className="Dashboard_Chart_Container_Item_Body">
                        <CanvasJSChart
                            className="Chart"
                            options={optionChartLabel}
                            containerProps={{
                                width: '100%',
                                height: '100%'
                            }}
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Charts;