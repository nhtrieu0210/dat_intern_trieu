import Dashboard1 from "../../../assets/image/dashboard-1.png";
function Welcome(props) {

    return (
        <>
            <div className="DB_Welcome Card">
                <div className="DB_Welcome_Container">
                    <div className="DB_Welcome_Container_Header">
                        <h1 className="DB_Welcome_Container_Header_Heading" onClick={()=> props.handleData_('Nhut Cuong')}>
                            Welcome to SB Admin Pro!
                        </h1>
                        <p className="DB_Welcome_Container_Header_Body">
                            Browse our fully designed UI toolkit! Browse our prebuilt app pages,
                            components,
                            and utilites, and be sure to look at our full documentation!
                        </p>
                    </div>
                    <div className="DB_Welcome_Container_Thumbnail">
                        <img src={Dashboard1} alt=""/>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Welcome;