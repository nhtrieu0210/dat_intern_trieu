import {FiChevronRight, FiList, FiMinusCircle, FiMoreVertical, FiPlusCircle} from "react-icons/fi";
import {useState} from "react";

function ProgressTracker() {
    const [open, setOpen] = useState(false)
    const handleOpen = () => {
        setOpen(!open);
    }

    return (
        <>
            <div className="DB_Tracker">
                <div className="DB_Tracker_Header">
                    <div className="DB_Tracker_Header_Title">Progress Tracker</div>
                    <button onClick={handleOpen} className="DB_Tracker_Header_Btn">
                        <FiMoreVertical size={14}/>
                        {open ? <div class="DB_Tracker_Header_Btn_Popup">
                            <a className="DB_Tracker_Header_Btn_Popup_Item">
                                <span className="Icon">
                                    <FiList />
                                </span>
                                <span className="Title">
                                    Manage Tasks
                                </span>
                            </a>
                            <a className="DB_Tracker_Header_Btn_Popup_Item">
                                <span className="Icon">
                                    <FiPlusCircle />
                                </span>
                                <span className="Icon">
                                    Add New Task
                                </span>
                            </a>
                            <a className="DB_Tracker_Header_Btn_Popup_Item">
                                <span className="Icon">
                                    <FiMinusCircle />
                                </span>
                                <span className="Icon">
                                    Delete Tasks
                                </span>
                            </a>
                        </div> : <></>}
                    </button>
                </div>
                <div className="DB_Tracker_Body">
                    <div className="DB_Tracker_Body_Container">
                        <h4 className="DB_Tracker_Body_Container_Title">
                            Server Migration
                            <span className="Percent">20%</span>
                        </h4>
                        <div className="DB_Tracker_Body_Container_Value">
                            <div className="Progress Danger Twenty"></div>
                        </div>
                    </div>
                    <div className="DB_Tracker_Body_Container">
                        <h4 className="DB_Tracker_Body_Container_Title">
                            Sale Tracking
                            <span className="Percent">40%</span>
                        </h4>
                        <div className="DB_Tracker_Body_Container_Value">
                            <div className="Progress Warning Forty"></div>
                        </div>
                    </div>
                    <div className="DB_Tracker_Body_Container">
                        <h4 className="DB_Tracker_Body_Container_Title">
                            Customer Database
                            <span className="Percent">60%</span>
                        </h4>
                        <div className="DB_Tracker_Body_Container_Value">
                            <div className="Progress Primary Sixty"></div>
                        </div>
                    </div>
                    <div className="DB_Tracker_Body_Container">
                        <h4 className="DB_Tracker_Body_Container_Title">
                            Payout Details
                            <span className="Percent">80%</span>
                        </h4>
                        <div className="DB_Tracker_Body_Container_Value">
                            <div className="Progress Info Eighty"></div>
                        </div>
                    </div>
                    <div className="DB_Tracker_Body_Container">
                        <h4 className="DB_Tracker_Body_Container_Title">
                            Account Setup
                            <span className="Percent">Complete</span>
                        </h4>
                        <div className="DB_Tracker_Body_Container_Value">
                            <div className="Progress Success One-Hundred"></div>
                        </div>
                    </div>

                </div>
                <div className="DB_Tracker_Footer">
                    <a className="DB_Tracker_Footer_Title">Visit Task Center</a>
                    <button className="DB_Tracker_Footer_Btn">
                        <FiChevronRight size={18} color={"#69707a"}/>
                    </button>
                </div>
            </div>
        </>
    );
};

export default ProgressTracker;