import {FiMoreVertical} from "react-icons/fi";
import {useState} from "react";
function RecentActivity() {
    const [open, setOpen] = useState(false)
    const handleOpen = () => {
        setOpen(!open);
    }

    return (
        <>
            <div className="DB_Recent">
                <div className="DB_Recent_Header">
                    <div className="DB_Recent_Header_Title">Recent Activity</div>
                    <button onClick={handleOpen} className="DB_Recent_Header_Btn">
                        <FiMoreVertical size={14} color={"#8f8f8f"}/>
                        {open ? <div class="DB_Recent_Header_Btn_Popup">
                            <h6 class="DB_Recent_Header_Btn_Popup_Heading">Filter Activity :</h6>
                            <a className="DB_Recent_Header_Btn_Popup_Item">
                                <span className="Success">
                                    Commerce
                                </span>
                            </a>
                            <a className="DB_Recent_Header_Btn_Popup_Item">
                                <span className="Primary">
                                    Reporting
                                </span>
                            </a>
                            <a className="DB_Recent_Header_Btn_Popup_Item">
                                <span className="Warning">
                                    Server
                                </span>
                            </a>
                            <a className="DB_Recent_Header_Btn_Popup_Item">
                                <span className="Secondary">
                                    Users
                                </span>
                            </a>
                        </div> : <></>}
                    </button>
                </div>
                <div className="DB_Recent_Body">
                    <div className="DB_Recent_Body_Container">
                        <div className="DB_Recent_Body_Container_Item">
                            <div className="DB_Recent_Body_Container_Item_Time">
                                <div className="Time">
                                    27 min
                                </div>
                                <div className="Dot Success"></div>
                            </div>
                            <div className="DB_Recent_Body_Container_Item_Desc">
                                New order placed! <a className="link-bold">Order #2912</a> has been successfully placed
                            </div>
                        </div>
                        <div className="DB_Recent_Body_Container_Item">
                            <div className="DB_Recent_Body_Container_Item_Time">
                                <div className="Time">
                                    58 min
                                </div>
                                <div className="Dot Primary"></div>
                            </div>
                            <div className="DB_Recent_Body_Container_Item_Desc">
                                Your <a className="link-bold">weekly report</a> has been generated and is
                                ready to view.
                            </div>
                        </div>
                        <div className="DB_Recent_Body_Container_Item">
                            <div className="DB_Recent_Body_Container_Item_Time">
                                <div className="Time">
                                    2 hrs
                                </div>
                                <div className="Dot Secondary"></div>
                            </div>
                            <div className="DB_Recent_Body_Container_Item_Desc">
                                New user <a className="link-bold">Valerie Luna</a> has registered
                            </div>
                        </div>
                        <div className="DB_Recent_Body_Container_Item">
                            <div className="DB_Recent_Body_Container_Item_Time">
                                <div className="Time">
                                    1 day
                                </div>
                                <div className="Dot Warning"></div>
                            </div>
                            <div className="DB_Recent_Body_Container_Item_Desc">
                                Server activity monitor alert
                            </div>
                        </div>
                        <div className="DB_Recent_Body_Container_Item">
                            <div className="DB_Recent_Body_Container_Item_Time">
                                <div className="Time">
                                    1 day
                                </div>
                                <div className="Dot Success"></div>
                            </div>
                            <div className="DB_Recent_Body_Container_Item_Desc">
                                New order placed! <a className="link-bold">Order #2912</a> has been successfully placed
                            </div>
                        </div>
                        <div className="DB_Recent_Body_Container_Item">
                            <div className="DB_Recent_Body_Container_Item_Time">
                                <div className="Time">
                                    1 day
                                </div>
                                <div className="Dot Secondary"></div>
                            </div>
                            <div className="DB_Recent_Body_Container_Item_Desc">
                                Details for <a className="link-bold">Marketing and Planning Meeting</a> have
                                been updated.
                            </div>
                        </div>
                        <div className="DB_Recent_Body_Container_Item">
                            <div className="DB_Recent_Body_Container_Item_Time">
                                <div className="Time">
                                    2 day
                                </div>
                                <div className="Dot Success"></div>
                            </div>
                            <div className="DB_Recent_Body_Container_Item_Desc">
                                New order placed! <a className="link-bold">Order #2912</a> has been successfully placed
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default RecentActivity;
