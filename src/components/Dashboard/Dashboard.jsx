import {signalSideBar} from "../Header/Header";

import {
    FiActivity, FiCalendar, FiMoreVertical
} from "react-icons/fi";
import {useEffect, useState} from "react";

import Welcome from "./Widgets/Welcome";
import RecentActivity from "./Widgets/RecentActivity";
import ProgressTracker from "./Widgets/ProgressTracker";
import Cards from "./Cards/Cards";


import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Charts from "./Charts/Charts";


function Dashboard() {
    let [isOpen, setOpen] = useState(false);
    const [startDate, setStartDate] = useState(new Date());

    useEffect(() => {
        signalSideBar.subscribe((value)=>{setOpen(value)})
    }, [signalSideBar]);


    let [isOpenDayPicker, setOpenDayPicker] = useState(false);

    const today = new Date();
    let options = { year: 'numeric', month: 'long', day: 'numeric' };
    let todayFormatted = today.toLocaleDateString('en-US', options);

    let handleOpenDayPicker = () => {
        setOpenDayPicker(!isOpenDayPicker);
    }

    return (
        <>
            <div id="dash"
                 className={`Dashboard ${isOpen ? '' : 'Full'}`}
            >
                <div className="Dashboard_Heading">
                    <div className="Dashboard_Heading_Content">
                        <div className="Dashboard_Heading_Content_Left">
                            <div className="Dashboard_Heading_Content_Left_Title">
                                <div className="icon">
                                    <FiActivity size={28} color="#86a4f3"/>
                                </div>
                                Dashboard
                            </div>
                            <div className="Dashboard_Heading_Content_Left_Subtitle">
                                Example dashboard overview and content summary
                            </div>
                        </div>
                        <div className="Dashboard_Heading_Content_Right">
                            <div onClick={handleOpenDayPicker} className="Calendar">
                                <div className="Calendar_Icon">
                                    <FiCalendar color={"#0061f2"} size={16}/>
                                </div>
                                <DatePicker
                                    className="Calendar_Input"
                                    selected={startDate} onChange={(date)=> setStartDate(date)}
                                    dateFormat="MMM dd, yyyy"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Dashboard_Widget">
                    <div className="Dashboard_Widget_Container">
                        <div className="Dashboard_Widget_Container_Item">
                            <Welcome/>
                        </div>
                        <div className="Dashboard_Widget_Container_Item">
                            <RecentActivity/>
                        </div>
                        <div className="Dashboard_Widget_Container_Item">
                            <ProgressTracker/>
                        </div>
                    </div>
                </div>
                <div className="Dashboard_Card">
                    <div className="Dashboard_Card_Container">
                        <Cards />
                    </div>
                </div>
                <div className="Dashboard_Chart">
                    <Charts />
                </div>
            </div>
        </>
    );
}

export default Dashboard;

